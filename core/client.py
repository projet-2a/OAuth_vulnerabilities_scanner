
class Client():
    def __init__(self, client_id: str, client_secret: str):
        """
        A client configuration.

        The configurated redirect_uri for the client must be the redirect uri of the scanner.

        :param client_id: client id string
        :param client_secret: client secret string
        """
        self.client_id = client_id
        self.client_secret = client_secret
