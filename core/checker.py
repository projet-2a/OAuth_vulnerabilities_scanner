from typing import List
import datetime
import jwt

from .token import Token
from .vulns import Vuln, SpecificVuln

jwt_valid_alg = ['HS256', 'HS384', 'HS512', 'ES256', 'ES384', 'ES512', 'RS256', 'RS384', 'RS512', 'PS256', 'PS384', 'PS512']


class Checker():

    def __init__(self, scanner):
        self.s = scanner

    def check_token(self, token: Token) -> List[Vuln]:
        ret: List[Vuln] = []

        if not token.expires_in:
            ret.append(Vuln("token_lifetime"))

        if token.expires_in > 3600:
            ret.append(Vuln("token_lifetime"))

        return ret

    def check_JWT(self, encoded_jwt, expire_in) -> List[Vuln]:
        ret: List[Vuln] = []

        if expire_in == None:
            return [SpecificVuln('No expire in value')]

        jwt_header = jwt.get_unverified_header(encoded_jwt)
        if not jwt_header:
            ret.append(SpecificVuln('Not valid JWT'))

        if jwt_header['typ'] != 'JWT':
            ret.append(SpecificVuln('This is not an JWT'))

        if jwt_header['alg'] not in jwt_valid_alg:
            ret.append(Vuln('token_sign_method'))

        try:
            jwt_data = jwt.decode(encoded_jwt, verify=False)

        except jwt.ExpiredSignatureError:
            ret.append(SpecificVuln('Token has expired', risk=0))

        except Exception as e:
            ret.append(SpecificVuln('Error: %s' % e, risk=10))

        else:
            if jwt_data['exp'] - jwt_data['iat'] != expire_in:
                ret.append(Vuln("token_expiration_date"))

        return ret
