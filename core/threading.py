from .introspect import Introspect
from .sessions import ImplicitGrant, ClientCredentailsGrant
from .client import Client
import threading
import os
import types
import json
from pathlib import Path
import importlib
import requests.exceptions

import logging
logger = logging.getLogger(__name__)


class Scanner_TH(threading.Thread):

    CONTROL_MODULE = "controls"

    def __init__(self, s):
        """

        :param s: Scanner
        """
        threading.Thread.__init__(self)
        self.s = s

    def _find_controls(self):
        current_path = Path()
        controls_path = current_path / Scanner_TH.CONTROL_MODULE

        ret = []
        if controls_path.exists() and controls_path.is_dir():
            for file in controls_path.iterdir():
                filename = file.name
                if filename.endswith(".py") and filename.count('.') == 1:
                    name = file.with_suffix('').name
                    ret.append("%s.%s" % (Scanner_TH.CONTROL_MODULE, name))

        else:
            raise Exception("Not controls folder")

        return ret

    def run(self):
        logger.info("scanner started")

        all_controls_path = self._find_controls()

        logger.debug("controls %s" % all_controls_path)
        self.s.emitter.controls_list(all_controls_path)

        for control_path in all_controls_path:
            c = importlib.import_module(control_path)
            if hasattr(c, 'run') and type(c.run) is types.FunctionType:
                # Run control
                self.s.emitter.control_start(control_path)

                try:
                    vulns = c.run(self.s)

                    self.s.emitter.control_end(control_path)
                    data = json.dumps([i.to_dict() for i in vulns])
                    logger.info("Result of %s : %s" % (control_path, data))
                    self.s.emitter.result(data)

                except requests.exceptions.SSLError:
                    logger.error("Controller raise SSLError")
                    self.s.emitter.control_error(control_path, "SSLError")

            else:
                logger.warning("%s invalid %s" % (control_path, dir(c)))

        # session = ImplicitGrant(self.s)
        # flow = session.request()
        # logger.info("Implicit flow: %s" % flow)
        # token = session.extract_token(flow)
        # logger.info("Implicit token: %s" % token)

        # if flow.responses[0].state != token.state:
        #     logger.warn("Vuln: state not match")

        # session = ClientCredentailsGrant(self.s)
        # flow = session.request()
        # logger.info("ClientCreds flow %s" % flow)
        # token = session.extract_token(flow)
        # logger.info("ClientCreds token %s" % token)

        # if flow.responses[0].status_code != 200:
        #     logger.warn("Vuln: Client creds return non 200")

        # if not token.expires_in:
        #     logger.warn("Vuln: no expires")

        # if not token.token:
        #     logger.warn("Vuln: no token")

        # if token.expires_in > 3600:
        #     logger.warn("Vuln: expires is too long")

        # intro = Introspect(self.s)
        # res = intro.request(token)
        # logger.info("Introspect res %s" % res)
        # v_token = intro.extract_token(res)
        # logger.info("Introspect token %s" % v_token)

        # TODO: client POC
        # c1 = Client("2793ac7c02fbd8c3c324", "0f29eed87feacbc3e6960d478a13e9d8")
        # c2 = self.s.get_default_client()
