import json
from typing import List

from .client import Client

class Config():

    @staticmethod
    def config_from_json(json_config: str) -> "Config":
        """
        {
            // Endpoints
            authorizationEndpoint: "",
            tokenEndpoint: "",
            introspectEndpoint: "",
            revokeEndpoint: "",
            // Grant types
            authorizationCodeGrant: false,
            implicitGrant: false,
            clientCredentialsGrant: false,
            resourceOwnerPasswordCredentialsGrant: false,
            // Clients
            clients: [
                {
                    clientID: "",
                    clientSecret: ""
                }
            ],
        }
        """
        data = json.loads(json_config)

        config = Config()

        config.authorize_endpoint = data.get('authorizationEndpoint', None)
        config.token_endpoint = data.get('tokenEndpoint', None)
        config.introspect_endpoint = data.get('introspectEndpoint', None)
        config.revoke_endpoint = data.get('revokeEndpoint', None)
        config.authorization_code_grant = data.get('authorizationCodeGrant', None)
        config.implicit_grant = data.get('implicitGrant', None)
        config.client_credentials_grant = data.get('clientCredentialsGrant', None)
        config.resource_owner_password_credentials_grant = data.get('resourceOwnerPasswordCredentialsGrant', None)
        
        config.resource_owner_name = data.get('resourceOwnerName', None)
        config.resource_owner_password = data.get('resourceOwnerPassword', None)
        
        config.clients = []
        clients = data.get('clients', [])
        if type(clients) is list:
            for c in clients:
                clientID = c.get('clientID', None)
                client_secret = c.get('clientSecret', None)
                if clientID and client_secret:
                    config.clients.append(Client(clientID, client_secret))

        return config

    def __init__(self, *args, **kargs):
        self.token_endpoint = "https://api.cyberoauth.tk/v1/token"
        self.authorize_endpoint = "https://api.cyberoauth.tk/v1/authorize"
        self.introspect_endpoint = "https://api.cyberoauth.tk/v1/introspect"
        self.revoke_endpoint = "https://api.cyberoauth.tk/v1/revoke"

        self.authorization_code_grant = True
        self.implicit_grant = True
        self.client_credentials_grant = True
        self.resource_owner_password_credentials_grant = True

        self.resource_owner_name = "user"
        self.resource_owner_password = "user"


        self.clients: List[Client] = []
        self.clients.append(Client("d74e426a560d6eadbb1b", "9d0e456addfbbadb90217d45eb92f55a"))
