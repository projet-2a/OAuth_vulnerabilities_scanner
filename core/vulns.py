import json
import os.path

with open(os.path.join('config', 'vulns.json')) as f:
    data = json.load(f)
    all_vulns = data['vulns']


class Vuln():
    def __init__(self, _id=None):

        vuln = all_vulns[_id]
        self.id = _id
        self.name = vuln["title"]
        self.description = vuln["description"]
        self.risk = vuln["risk"]
        self.vuln = vuln

        if type(self.risk) == str:
            self.risk = int(self.risk)

    def to_dict(self):
        return {
            'id': self.name,
            'description': self.description,
            'risk': self.risk,
            'url': self.vuln['url'],
            'title': self.vuln['title']
        }

    def __str__(self):
        return str(self.vuln)


class SpecificVuln():
    def __init__(self, name=None, description=None, risk=None, url=''):
        self.name = name
        self.description = description
        self.risk = risk
        self.url = url

        if risk == None:
            raise Exception("Vuln can t be specific and risk == None")

        if type(self.risk) == str:
            self.risk = int(self.risk)

    def to_dict(self):
        return {
            'id': self.name,
            'description': self.description,
            'risk': self.risk,
            'url': self.url,
            'title': self.name
        }

    def __str__(self):
        return "SpecificVuln(%s)" % self.name
