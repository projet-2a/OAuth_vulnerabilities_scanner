from .flow import Response
from .token import Token, ValidatedToken
import requests
from typing import Optional
import logging
logger = logging.getLogger(__name__)


class Introspect():
    def __init__(self, s):
        self.s = s

    def request(self, token: Token) -> Response:
        """
        Valid the token with the endpoint
        :param token: token object from Session request (code grant, implicit, client credentials, resource owner password)
        :type token: Token
        :return: endpoint response after request
        :rtype: Response
        """
        c = self.s.get_default_client()
        url = self.s.config.introspect_endpoint

        res = self.s.requester.post(url, data={"token": token.token, "token_type_hint": "access_token"}, client=c)
        response = Response(res.url, res.headers, status_code=res.status_code, data=res.text, method="POST")
        return response

    def extract_token(self, res: Response) -> Optional[ValidatedToken]:
        """
        Extract token from the answer (by Response)
        :param res: response from request()
        :type res: Response
        :rtype: ValidatedToken
        """

        data = res.json()
        if res.status_code != 200:
            logger.error("Introspect Error: %s" % data)
            raise Exception('Introspect Error: %s' % data)

        else:
            ret = ValidatedToken()
            ret.active = data.get('active', None)
            ret.scope = data.get('scope', None)
            ret.client_id = data.get('client_id', None)
            ret.username = data.get('username', None)
            ret.token_type = data.get('token_type', None)
            ret.exp = data.get('exp', None)
            ret.iat = data.get('iat', None)
            ret.nbf = data.get('nbf', None)
            ret.sub = data.get('sub', None)
            ret.aud = data.get('aud', None)
            ret.iss = data.get('iss', None)
            ret.jti = data.get('jti', None)

            return ret
