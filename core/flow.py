from typing import List, Dict, Any
import json

class Response():
    def __init__(self, url: str, headers: Dict[str, str], status_code: int=None, data: str=None, method: str="GET", state: str=None):
        """
        All useful information of a authorization or token request.
        :param url: the url
        :param headers: all headers
        :param status_code: the status_code
        :param data: the body data
        :param method: the http method (verb)
        :param state: the state used during the authorization request
        """
        self.url = url
        self.headers = headers
        self.data = data
        self.status_code = status_code
        self.method = method
        self.state = state

        self._extract_headers()

    def _extract_headers(self):
        """
        Extract useful headers from response.

        Extract 'Content-Type' in self.content_type

        :example: Headers example
            {'Host': 'localhost:5000', 'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0', 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', 'Accept-Language': 'fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3', 'Accept-Encoding': 'gzip, deflate', 'Connection': 'keep-alive', 'Cookie': 'session=eyJPQU5UX1NUQVRFIjoidzJyWTVVT1F0eSJ9.XDtHMA.9g5t2PjFqnACP6b28IM5W4gqdvo; io=LzYjxRjHQt7_hTMAAAAA', 'Upgrade-Insecure-Requests': '1', 'Cache-Control': 'max-age=0'}
        """

        self.content_type = None
        if 'Content-Type' in self.headers:
            self.content_type = self.headers['Content-Type']

    def json(self) -> Dict[str, Any]:
        """
        Return body as json if there is data in body and 'Content-Type' contain 'application/json'.
        """
        if not self.data:
            raise Exception("No data")
        if "application/json" not in self.content_type:
            raise Exception("Content-Type not json")
        
        return json.loads(self.data)


    def text(self) -> str:
        """
        Return body as string if there is data in body.
        """
        if not self.data:
            raise Exception("No data")
        return self.data

    def __repr__(self):
        return "Response(%s, %s, %s)" % (self.url, self.status_code, self.data)


class Flow():
    """
    A Flow is a sequence of authorization or token responses.

    Responses are in the `responses` attributes.
    """
    def __init__(self):
        self.responses: List[Response] = []

    def add_response(self, res: Response):
        """
        Add a response to the flow
        """
        self.responses.append(res)

    def __repr__(self):
        return "Flow[%s]" % ', '.join(map(repr, self.responses))

    def __len__(self):
        return len(self.responses)