import requests


class Requester():

    def __init__(self, verify_cert=True):
        self.verify = verify_cert

    def post(self, url, data, client=None):
        auth = None
        if client:
            auth = (client.client_id, client.client_secret)

        return requests.post(url, data=data, auth=auth, verify=self.verify)
