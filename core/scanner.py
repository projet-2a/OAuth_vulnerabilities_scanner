from .config import Config
from .client import Client
from .requester import Requester
from .threading import Scanner_TH
from .emitter import CoreEmitter
import threading
from typing import Dict, Any
import logging
logger = logging.getLogger(__name__)


class Scanner():
    """
    This object represents the instance of the scanner.

    It contains all the configurations and all the effectuated controls.
    """

    def __init__(self):
        self.running = False

        self.config: Config = None
        self.emitter: CoreEmitter = None
        self.requester: Requester = None
        self.serial = Serial()

    def set_config_json(self, data: str):
        """
        Set scanner configurations from json string.
        """
        c = Config.config_from_json(data)
        self.set_config(c)


    def set_config(self, config: Config):
        """
        Set scanner configurations.
        """
        self.config = config
        self.requester = Requester(verify_cert=True)  # TODO: verify_cert from config


    def create_sub(self, name: str) -> "SubScanner":
        # TODO
        pass

    def get_default_client(self):
        """
        Return default client from Config
        :rtype: Client
        """
        return self.config.clients[0]

    def get_redirect_url(self):
        """
        Return scanner callback url.
        """
        return "http://localhost:5000/callback"

    def set_emitter(self, emitter):
        """
        Configure emitter object from socket.io.
        """
        self.emitter = CoreEmitter(self, emitter)

    def start(self):
        """
        Start the scanner in new  python Thread.
        """
        if not self.config:
            raise Exception('Not configured')

        t = Scanner_TH(self)

        self.running = True
        t.start()


class SubScanner():
    def __init__(self, parameter_list):
        pass


class Serial():
    """Fake Async"""

    def __init__(self):
        self.result = None
        self.set = False
        self.eventObj = threading.Event()

    def set_result(self, result):
        self.result = result
        self.set = True

    def get_result(self):
        self.set = False
        ret = self.result
        self.result = None
        return ret

    def is_set(self):
        return self.set

    def play(self):
        self.eventObj.set()

    def pause(self):
        # Clear result
        self.get_result()
        self.eventObj.clear()
        self.eventObj.wait()
