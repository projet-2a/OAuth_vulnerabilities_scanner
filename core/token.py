from datetime import datetime, timedelta
import json

class Token():
    def __init__(self):
        self.raw_data = None
        self.token = None # str , value of useful the token
        self.expires_at = None
        self.expires_in = None
        self.refresh_token = None
        self.token_type = None
        self.scope = None
        self.state = None

    def extract_from_data(self, data):
        if not data:
            raise Exception("Data must not be None")

        self.raw_data = data
        self.token = data.get('access_token', None)
        self.token_type = data.get('token_type', None)
        self.refresh_token = data.get('refresh_token', None)
        self.scope = data.get('scope', None)

        if 'expires_in' in data:
            exp = data['expires_in']

            if type(exp) is str and exp.isdigit():
                exp = int(exp)

            if type(exp) is int:
                self.expires_in = exp

                now = datetime.now()
                expires = timedelta(seconds=exp)
                final = now + expires
                self.expires_at = final.replace(microsecond=0)

    def __repr__(self):
        return "Token %s (%s, %s, %s, %s)" % (self.token, self.token_type, self.scope, self.expires_at, self.refresh_token)


class ValidatedToken():
    def __init__(self):
        self.token: Token = None
        self.active = None
        self.scope = None
        self.client_id = None
        self.username = None
        self.token_type = None
        self.exp = None  # Expiration
        self.iat = None  # Issue at (token creation date)
        self.nbf = None  # Not before
        self.sub = None  # subject (id user)
        self.aud = None  # Audience
        self.iss = None  # Issuer (id server auth)
        self.jti = None  # Token id

    def __repr__(self):
        d = {
            "active": self.active,
            "scope": self.scope,
            "client_id": self.client_id,
            "username": self.username,
            "token_type": self.token_type,
            "exp": self.exp,
            "iat": self.iat,
            "nbf": self.nbf,
            "sub": self.sub,
            "aud": self.aud,
            "iss": self.iss,
            "jti": self.jti
        }
        return "VToken %s" % json.dumps(d)
