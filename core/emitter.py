import logging

class CoreEmitter():

    def __init__(self, s, emitter):
        self.s = s
        self.emitter = emitter

    def request_authorization(self, data, callback=None):
        self.emitter.emit_authorization(data, callback=callback)

    def controls_list(self, controls_list):
        self.emitter.emit_controls_list(controls_list)

    def control_start(self, control_name):
        self.emitter.emit_control_start(control_name)

    def control_end(self, control_name):
        self.emitter.emit_control_end(control_name)

    def control_error(self, control_name, error_name):
        data = {
            "control_name": control_name,
            "error_name": error_name
        }
        self.emitter.emit_control_error(data)

    def log(self, data):
        self.emitter.emit_log(data)

    def result(self, data):
        self.emitter.emit_result(data)

class LoggerEmitter(CoreEmitter):

    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def request_authorization(self, data, callback=None):
        self.logger.info("request_authorization %s" % data)

    def controls_list(self, controls_list):
        self.logger.info("controls_list %s" % controls_list)

    def control_start(self, control_name):
        self.logger.info("control_start %s" % control_name)

    def control_end(self, control_name):
        self.logger.info("control_end %s" % control_name)

    def control_error(self, control_name, error_name):
        data = {
            "control_name": control_name,
            "error_name": error_name
        }
        self.logger.info("control_error %s" % data)

    def log(self, data):
        self.logger.info("log %s" % data)

    def result(self, data):
        self.logger.info("result %s" % data)