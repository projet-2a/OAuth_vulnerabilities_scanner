from core.introspect import Introspect
from core.sessions import ClientCredentailsGrant

from core.vulns import SpecificVuln

def run(s):

    vulns = []
    client_id = s.get_default_client().client_id

    session = ClientCredentailsGrant(s)
    flow = session.request()
    token = session.extract_token(flow)

    intro = Introspect(s)
    res = intro.request(token)
    v_token = intro.extract_token(res)

    if not v_token.active:
        vulns.append(SpecificVuln("Introspect endpoint not validate valid token", risk=7))

    if v_token.client_id != client_id:
        vulns.append(SpecificVuln("Introspect endpoint return bad client_id", risk=7))

    return vulns