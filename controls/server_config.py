from core.vulns import Vuln

def run(s):
    vulns = []

    if not s.config.token_endpoint.startswith("https://"):
        vulns.append(Vuln("token_no_transmitted_using_tls"))

    if not s.config.authorize_endpoint.startswith("https://"):
        vulns.append(Vuln("token_no_transmitted_using_tls"))

    return vulns
