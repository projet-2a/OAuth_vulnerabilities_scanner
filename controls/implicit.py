from core.sessions import ImplicitGrant
from core.vulns import SpecificVuln, Vuln
from core.checker import Checker


def run(s):

    vulns = []
    c = Checker(s)

    session = ImplicitGrant(s)
    flow = session.request()
    token = session.extract_token(flow)

    vulns += c.check_token(token)
    vulns += c.check_JWT(token.token, token.expires_in)

    return vulns
