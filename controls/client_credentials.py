from core.checker import Checker
from core.vulns import Vuln, SpecificVuln
from core.sessions import ClientCredentailsGrant


def run(s):
    """
    run the test for client credential
    :param s: Scanner
    :return: Vuln[]
    """
    vulns = []
    c = Checker(s)

    session = ClientCredentailsGrant(s)
    flow = session.request()
    token = session.extract_token(flow)

    if flow.responses[0].status_code != 200:
        vulns.append(
            SpecificVuln("Bad status", "status_code for get token != 200, here %d" % flow.responses[0].status_code, risk=5))

    vulns += c.check_token(token)
    vulns += c.check_JWT(token.token, token.expires_in)

    return vulns
