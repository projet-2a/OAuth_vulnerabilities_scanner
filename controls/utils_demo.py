from core.introspect import Introspect
from core.sessions import ClientCredentailsGrant

from core.vulns import SpecificVuln, Vuln

def run(s):

    vulns = [
        SpecificVuln("Introspect endpoint not validate token", risk=7, url="https://blog.cyberoauth.tk/vulnerabilities/introspect_validation"),
        Vuln("token_no_transmitted_using_tls"),
        Vuln("token_sign_method"),
        Vuln("code_lifetime"),
        Vuln("token_expiration_date")
    ]

    return vulns