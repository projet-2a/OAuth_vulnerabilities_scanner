import logging
import os

from server.app import init
from core.scanner import Scanner


def config_logger():
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)

    log = logging.getLogger('engineio.server')
    log.setLevel(logging.ERROR)

    log = logging.getLogger('socketio.server')
    log.setLevel(logging.INFO)

    logging.basicConfig(format='[%(asctime)s] %(levelname)s in %(name)s: %(message)s', level=logging.DEBUG)


if __name__ == "__main__":

    config_logger()
    s = Scanner()

    # disable flask server banner
    os.environ['WERKZEUG_RUN_MAIN'] = 'true'

    app, socketio = init(s)
    app.debug = True
    app.env = "development"
    app.secret_key = "leperou"

    logging.info('Server started on http://localhost:5000')
    socketio.run(app)
