import core.scanner
import core.sessions
import core.config
import core.emitter

s = core.scanner.Scanner()
s.set_config(core.config.Config())
s.set_emitter(core.emitter.LoggerEmitter())

session = core.sessions.ResourceOwnerPasswordCredentialsGrant(s)

flow = session.request()
token = session.extract_token(flow)

print(flow)
print(token)
