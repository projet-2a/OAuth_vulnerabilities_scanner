# OAnt Vulnerabilities Scanner

## Install & Run

Create virtualenv

```
pip install -r requirements.txt
```

Run

```
python main.py
```

## Controls

All the scanner controls are in `controls/` folder.

-   client_credentials.py: test client credentials grant type
-   introspect_token.py: test introspection endpoint
-   server_config.py: test the server information, for example if the url is `https`

## Specs

### Events

-   authorize: Ask for authorization code or token, give a url to open and a callback. When the resource owner give the access, the callback must be called with the full redirection url (including query params and hash)  
    Data: Url and a callback

-   log: Contain a scanner log, Not used

-   progress: Not used

-   result: Emitted when a control end, contains a list of vulnerabilities detected by the scanner.  
    Data: List of Vulnerability
    Example:

    ```
    [{
    "id": "Too big token lifetime",
    "description": "The introspect endpoint ...",
    "risk": "5",
    "url": "https://example.com/vulns/token_lifetime",
    "title": "Too big token lifetime"
    },
    ...
    ]
    ```

-   controls_list: Emitted when the scanner start constain a list of all controls.  
    Data: List of control name

-   control_start: Emitted when the scanner execute a new control python file.  
    Data: Control name

-   control_end: Emitted when a control terminate this execution.  
    Data: Control name

-   control_error: Emitted when a error occurs during control execution.  
    Data: { "control_name": Control name, "error_name": Error name }

### API

Framework short references

#### Scanner Object

The Scanner object represents the current scan instance, it contains all the information about the current scan.

This object is passed to the `control` in params of the `run` function.
It is used to instantiates many object like Sessions, Introspect...

Attributes:

-   Scanner.config: Scanner.Config  
    Contains configuration

-   Scanner.emitter: Scanner.Emitter  
    Allow to communicate with the webclient interface

-   Scanner.requester: Requester  
    Allow to make http request

#### Grant types

The 4 main grant types utils functions are in `core.sessions`

-   class ImplicitGrant
-   class AuthorizationCodeGrant
-   class ClientCredentailsGrant
-   class ResourceOwnerPasswordCredentialsGrant

All this classes have the same functions:

-   init(s: Scanner)
-   request(): Flow
-   extract_token(f: Flow): Token
