class Emitter():

    AUTHORIZE_EVENT = "authorize"
    LOG_EVENT = "log"
    PROGRESS_EVENT = "progress"
    RESULT_EVENT = "result"
    CONTROLS_LIST_EVENT = "controls_list"
    CONTROL_START_EVENT = "control_start"
    CONTROL_END_EVENT = "control_end"
    CONTROL_ERROR_EVENT = "control_error"

    def __init__(self, sio):
        self.sio = sio

    def emit_authorization(self, data, callback=None):
        self.sio.emit(Emitter.AUTHORIZE_EVENT, data, callback=callback)

    def emit_log(self, data, callback=None):
        self.sio.emit(Emitter.LOG_EVENT, data, callback=callback)

    def emit_progress(self, data, callback=None):
        self.sio.emit(Emitter.PROGRESS_EVENT, data, callback=callback)

    def emit_result(self, data, callback=None):
        self.sio.emit(Emitter.RESULT_EVENT, data, callback=callback)

    def emit_controls_list(self, data, callback=None):
        self.sio.emit(Emitter.CONTROLS_LIST_EVENT, data, callback=callback)

    def emit_control_start(self, data, callback=None):
        self.sio.emit(Emitter.CONTROL_START_EVENT, data, callback=callback)

    def emit_control_end(self, data, callback=None):
        self.sio.emit(Emitter.CONTROL_END_EVENT, data, callback=callback)

    def emit_control_error(self, data, callback=None):
        self.sio.emit(Emitter.CONTROL_ERROR_EVENT, data, callback=callback)
