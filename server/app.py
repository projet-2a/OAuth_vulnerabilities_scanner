import gevent.monkey
gevent.monkey.patch_all()

from .emitter import Emitter
from core.scanner import Scanner
from core.flow import Response
from flask import Flask, render_template, request, send_from_directory

from flask_socketio import SocketIO
import threading
import logging
logger = logging.getLogger(__name__)


def init(scanner: Scanner):

    app = Flask(__name__)
    sio = SocketIO(app)

    scanner.set_emitter(Emitter(sio))

    @sio.on('connect')
    def connect():
        logger.info('connect')

    @sio.on('disconnect')
    def disconnect():
        logger.info('disconnect')

    @sio.on('run')
    def run(json_config):
        logger.info('run config: %s' % json_config)

        scanner.set_config_json(json_config)
        scanner.start()

    @sio.on('authorize')
    def authorize(url):
        logger.info("authorize %s" % url)

        if scanner.serial.is_set():
            logger.debug("auth_callback: set final response")
            res = scanner.serial.get_result()
            res.url = url
            scanner.serial.set_result(res)
            scanner.serial.play()
        else:
            logger.debug("auth_callback: set url")
            scanner.serial.set_result(url)

    @app.route('/')
    def index():
        """Serve the client-side application index"""
        return send_from_directory('frontend', 'index.html')

    @app.route('/<path:path>')
    def send_js(path):
        """Serve the client-side application."""
        return send_from_directory('frontend', path)

    @app.route('/callback')
    def callback():
        response = Response(request.url, dict(request.headers))
        if scanner.serial.is_set():
            logger.debug("flask_callback: set final response")
            url = scanner.serial.get_result()
            response.url = url
            scanner.serial.set_result(response)
            scanner.serial.play()
        else:
            logger.debug("flask_callback: set partial response")
            scanner.serial.set_result(response)

        return render_template('callback.html')

    return app, sio
